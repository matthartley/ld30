Player = class()

function Player:construct ()

	self.x = 0
	self.y = 0
	self.health = 12
	self.hurtCooldown = 30
	self.dead = false
	self.redCooldown = 0
	self.hitflashCooldown = 0

	self.animationFrame = 0
	self.animationTimer = 0
	self.animate = false

	self.attackAnimationFrame = 0
	self.attackAnimationTimer = 0
	self.attackAnimate = false

	self.attackCooldown = 0

	self.sy = 0
end

function Player:render ()

	local z = -(self.y/100)

	video.color(1, 1, 1, 1)
	if self.redCooldown > 0 then video.color(1, 0, 0, 1) end
	--video.renderSprite(texture.characters, 32+16*self.animationFrame, self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 4)
	
	--if self.hitflashCooldown > 0 then self:renderHitflash() end

	if self.attackCooldown > 0 then video.renderSpritez(texture.actors, self.sy, 128, 16, 16, self.x+offset.x, self.y+offset.y, 4, z)
	else video.renderSpritez(texture.actors, 16*self.animationFrame, 64+self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 4, z) end

	video.renderSpritez(texture.actors, 16*self.animationFrame, self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 4, z)

	video.color(1, 1, 1, 0.5)
	video.renderSpritez(texture.actors, 0, 128+16, 16, 16, self.x+offset.x, (self.y+offset.y)+8, 4, z)
end

function Player:renderHitflash ()

	video.color(1, 1, 1, 1)

	if self.sy == 32 then
		video.renderSprite(texture.characters, 32, 64, 16, 16, (self.x+offset.x)+72, self.y+offset.y+16, 4)
	end
	if self.sy == 48 then
		video.renderSprite(texture.characters, 48, 64, 16, 16, (self.x+offset.x)-32, self.y+offset.y+16, 4)
	end
end

function Player:tick ()

	if self.attackAnimate then
		self.attackAnimationTimer = self.attackAnimationTimer + 1
		if self.attackAnimationTimer > 3 then
			self.attackAnimationTimer = 0
			self.attackAnimationFrame = self.attackAnimationFrame + 1
		end
		if self.attackAnimationFrame > 2 then
			self.attackAnimationFrame = 0
			self.attackAnimate = false
		end
	end

	if key[keyCode.up] or key[keyCode.down] or key[keyCode.left] or key[keyCode.right] then
		self.animate = true
	else
		self.animate = false
	end

	if self.animate then self.animationTimer = self.animationTimer + 1 end
	if self.animationTimer > 10 then
		self.animationTimer = 0
		self.animationFrame = self.animationFrame + 1

		if self.animationFrame == 2 or self.animationFrame == 5 then
			audio.play(sound.step[math.random(3)])
		end
	end
	if self.animationFrame > 5 then
		self.animationFrame = 0
	end

	if not self.animate then
		self.animationFrame = 0
		self.animationTimer = 0
	end

	if key[keyCode.up] then
		self.y = self.y - 3
		self.sy = 16
	end
	if key[keyCode.down] then
		self.y = self.y + 3
		self.sy = 0
	end
	if key[keyCode.left] then
		self.x = self.x - 3
		self.sy = 48
	end
	if key[keyCode.right] then
		self.x = self.x + 3
		self.sy = 32
	end

	self:tileCollisions()

	if self.attackCooldown > 0 then self.attackCooldown = self.attackCooldown - 1 end
	if self.hitflashCooldown > 0 then self.hitflashCooldown = self.hitflashCooldown - 1 end
	if self.redCooldown > 0 then self.redCooldown = self.redCooldown - 1 end
	if self.hurtCooldown > 0 then self.hurtCooldown = self.hurtCooldown - 1 end

	if not self.dead then
		if self.hurtCooldown <= 0 then
			for i, enemy in ipairs(enemies) do
				if self.x < enemy.x+32 and self.x+32 > enemy.x and self.y < enemy.y+32 and self.y+32 > enemy.y then
					self.health = self.health - 1
					self.hurtCooldown = 30
					self.redCooldown = 10
					if self.health <= 0 then self.dead = true end
					audio.play(sound.hurt)
				end
			end
		end
	end

	-- PORTAL
	if portal ~= nil then
		if self.x < (portal.x+24)+96 and self.x+96 > (portal.x+24) and self.y < (portal.y+24)+96 and self.y+96 > (portal.y+24) then
			nextLevel()
			audio.play(sound.portal)
		end
	end
end

function Player:tileCollisions ()

	for i, tile in ipairs(tiles) do
		if tile.raised then
			if self.x < tile.x + 48 and
				self.x + 32 > tile.x and
				self.y < tile.y + 24 and
				self.y + 64 > tile.y then

				local leftdist = self.x+32 - tile.x
				local rightdist = tile.x+48 - self.x
				local topdist = self.y+64 - tile.y
				local bottomdist = tile.y+24 - self.y

				if leftdist < rightdist and leftdist < topdist and leftdist < bottomdist then
					self.x = tile.x - 32
				end
				if rightdist < leftdist and rightdist < topdist and rightdist < bottomdist then
					self.x = tile.x + 48
				end
				if topdist < leftdist and topdist < rightdist and topdist < bottomdist then
					self.y = tile.y - 64
				end
				if bottomdist < leftdist and bottomdist < rightdist and bottomdist < topdist then
					self.y = tile.y + 24
				end

			end
		end
	end

	-- MAP EDGES
	if self.x < 0 then self.x = 0 end
	if self.x + 64 > 3072 then self.x = 3072 - 64 end
	if self.y < 1 then self.y = 1 end
	if self.y + 64 > 720 then self.y = 720 - 64 end
end

function Player:attack ()

	self.attackAnimate = true
	self.attackCooldown = 10

	for i, enemy in ipairs(enemies) do
		if self.sy == 32 then
			if self.x < enemy.x and self.x+80 > enemy.x and self.y < enemy.y+32 and self.y+32 > enemy.y then
				enemy:hurt(3)
				self.hitflashCooldown = 5
				audio.play(sound.hit)
			end
		end
		if self.sy == 48 then
			if self.x < enemy.x+48 and self.x > enemy.x and self.y < enemy.y+32 and self.y+32 > enemy.y then
				enemy:hurt(3)
				self.hitflashCooldown = 5
				audio.play(sound.hit)
			end
		end

		if self.sy == 0 then
			if self.x < enemy.x+32 and self.x+32 > enemy.x and self.y < enemy.y and self.y+48 > enemy.y then
				enemy:hurt(3)
				self.hitflashCooldown = 5
				audio.play(sound.hit)
			end
		end
		if self.sy == 16 then
			if self.x < enemy.x+32 and self.x+32 > enemy.x and self.y < enemy.y+48 and self.y > enemy.y then
				enemy:hurt(3)
				self.hitflashCooldown = 5
				audio.play(sound.hit)
			end
		end
	end
end

function Player:resetPosition ()

	self.x = 134
	self.y = (720/2)-56
end