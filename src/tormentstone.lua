TormentStone = class()

function TormentStone:construct (x, y)

	self.x = x*48
	self.y = y*48
end

function TormentStone:render ()

	local z = -((self.y-24)/100)
	video.color(1, 1, 1, 1)
	video.renderSpritez(texture.tiles, 64, 16, 16, 16, self.x+offset.x, self.y+offset.y, 3, z)
end