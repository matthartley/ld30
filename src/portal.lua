Portal = class()

function Portal:construct (x, y)

	self.x = x*48
	self.y = y*48

	self.animationFrame = 0
	self.animationTimer = 0

	self.particles = {}
	self.particleSpawn = 3
end

function Portal:render ()

	local z = -((self.y-10)/100)

	video.enableTextures(false)
	video.color(105/255, 210/255, 231/255, 1)
	for i, part in ipairs(self.particles) do
		video.renderQuadz(self.x+offset.x+part.x+21, self.y+offset.y+part.y+27, 4, 4, z)
	end

	video.color(1, 1, 1, 1)
	video.renderSpritez(texture.portal, 24*self.animationFrame, 24, 24, 24, (self.x+offset.x)-8, (self.y+offset.y)-16, 3, z)
end

function Portal:tick ()

	self.animationTimer = self.animationTimer + 1
	if self.animationTimer > 8 then
		self.animationTimer = 0
		self.animationFrame = self.animationFrame + 1
	end
	if self.animationFrame > 2 then
		self.animationFrame = 0
	end

	if self.particleSpawn > 0 then self.particleSpawn = self.particleSpawn - 1 end
	if self.particleSpawn <= 0 then
		self:spawnParticle()
		self.particleSpawn = 3
	end

	-- PARTICLES
	for i, p in ipairs(self.particles) do
		local d = math.sqrt((p.x*p.x) + (p.y*p.y))
		if p.x > 0 then p.x = p.x - ((192-d)*0.02) end
		if p.x < 0 then p.x = p.x + ((192-d)*0.02) end
		if p.y > 0 then p.y = p.y - ((192-d)*0.02) end
		if p.y < 0 then p.y = p.y + ((192-d)*0.02) end

		-- if p.x > 0 then p.x = p.x + (64-p.x)*0.1
		-- else p.x = p.x + (64-p.x)*0.1 end
		-- if p.y > 0 then p.y = p.y + (64-p.y)*0.1
		-- else p.y = p.y + (64-p.y)*0.1 end

		if p.x < 8 and p.x > -8 and p.y < 8 and p.y > -8 then
			table.remove(self.particles, i)
			i = i - 1
		end
	end
end

function Portal:spawnParticle ()

	table.insert(self.particles, { x = math.random(256)-128, y = math.random(256)-128 })
end