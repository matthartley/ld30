Tile = class()

tileRows = {
	8,
	4,
	4,
	4,
	4
}

function Tile:construct (x, y, row, raised)

	self.x = x*48
	self.y = y*48
	--print('ROW',tileRows[row+1])
	self.sx = math.random(tileRows[row+1]) - 1
	self.sy = row * 16
	self.raised = raised
end

function Tile:render ()
	
	video.color(1, 1, 1, 1)
	if self.x+offset.x < 1280 and self.x+offset.x+48 > 0 and self.y+offset.y < 720 and self.y+offset.y+48 > 0 then
		video.renderSprite(texture.tiles, self.sx*16, self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 3)
	end
	-- if self.dirt then
	-- 	video.renderSprite(texture.tiles, 32, 0, 8, 16, self.x+offset.x, (self.y+offset.y)-48, 6)
	-- end
end

function Tile:tick ()


end