font = {}

font.characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ      abcdefghijklmnopqrstuvwxyz'

function font.render (string, x, y, scale)

	for i = 1, #string do
		local c = string:sub(i, i)
		local s, e = string.find(font.characters, c, 1, true)
		s = s - 1
		--print('s '..s)
		video.color(1, 1, 1, 1)
		video.renderSpritez(texture.font, ((s)%8)*8, (math.floor(s/8))*8, 8, 8, x+((i-1)*(scale*8)), y, scale, -10)
	end
end