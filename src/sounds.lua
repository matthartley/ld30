sound = {
	step = {
		audio.loadAudio(respath..'sound/step6.wav'),
		audio.loadAudio(respath..'sound/step2.wav'),
		audio.loadAudio(respath..'sound/step3.wav')
	},
	hit = audio.loadAudio(respath..'sound/hit.wav'),
	hurt = audio.loadAudio(respath..'sound/hurt.wav'),
	portal = audio.loadAudio(respath..'sound/portal.wav'),
}