Enemy = class()

function Enemy:construct (x, y)

	self.x = x
	self.y = y
	self.health = 12
	self.redCooldown = 0
	self.hurtCooldown = 30
	self.dead = false

	self.animationFrame = 0
	self.animationTimer = 0
	self.animate = true

	self.sy = 0
end

function Enemy:render ()

	local z = -(self.y/100)

	-- SWORD
	video.renderSpritez(texture.actors, 16*self.animationFrame, 64+self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 4, z)

	video.color(1, 1, 1, 1)
	if self.redCooldown > 0 then video.color(1, 0, 0, 1) end
	video.renderSpritez(texture.actors, 16*self.animationFrame, 160+self.sy, 16, 16, self.x+offset.x, self.y+offset.y, 4, z)

	-- SHADOW
	video.color(1, 1, 1, 0.5)
	video.renderSpritez(texture.actors, 0, 128+16, 16, 16, self.x+offset.x, (self.y+offset.y)+8, 4, z)
end

function Enemy:tick ()

	if self.animate then self.animationTimer = self.animationTimer + 1 end
	if self.animationTimer > 10 then
		self.animationTimer = 0
		self.animationFrame = self.animationFrame + 1
	end
	if self.animationFrame > 5 then
		self.animationFrame = 0
	end

	if self.x+offset.x < 1280 and self.x+offset.x+32 > 0 and self.y+offset.y < 720 and self.y+offset.y+32 > 0 then
		local xdir = 0
		local ydir = 0

		if self.x < player.x+32 and self.x+32 > player.x then
		else

			if player.x > self.x then
				xdir = 1
			elseif player.x < self.x then
				xdir = -1
			end

			self.x = self.x + 1.5*xdir
		end

		if self.y < player.y+32 and self.y+32 > player.y then
		else

			if player.y > self.y then
				ydir = 1
			elseif player.y < self.y then
				ydir = -1
			end

			self.y = self.y + 1*ydir
		end

		if ydir == 1 then self.sy = 0 end
		if ydir == -1 then self.sy = 16 end
		if xdir == 1 then self.sy = 32 end
		if xdir == -1 then self.sy = 48 end
	end

	self:tileCollisions()

	if self.redCooldown > 0 then self.redCooldown = self.redCooldown - 1 end
	if self.hurtCooldown > 0 then self.hurtCooldown = self.hurtCooldown - 1 end

	-- MAP EDGES
	if self.x < 0 then self.x = 0 end
	if self.x + 64 > 3072 then self.x = 3072 - 64 end
	if self.y < 1 then self.y = 1 end
	if self.y + 64 > 720 then self.y = 720 - 64 end
end

function Enemy:tileCollisions ()

	for i, tile in ipairs(tiles) do
		if tile.raised then
			if self.x < tile.x + 48 and
				self.x + 32 > tile.x and
				self.y < tile.y + 24 and
				self.y + 64 > tile.y then

				local leftdist = self.x+32 - tile.x
				local rightdist = tile.x+48 - self.x
				local topdist = self.y+64 - tile.y
				local bottomdist = tile.y+24 - self.y

				if leftdist < rightdist and leftdist < topdist and leftdist < bottomdist then
					self.x = tile.x - 32
				end
				if rightdist < leftdist and rightdist < topdist and rightdist < bottomdist then
					self.x = tile.x + 48
				end
				if topdist < leftdist and topdist < rightdist and topdist < bottomdist then
					self.y = tile.y - 64
				end
				if bottomdist < leftdist and bottomdist < rightdist and bottomdist < topdist then
					self.y = tile.y + 24
				end

			end
		end
	end

	for i, other in ipairs(enemies) do
			if self.x < other.x + 32 and
				self.x + 32 > other.x and
				self.y < other.y + 32 and
				self.y + 32 > other.y then

				local leftdist = self.x+32 - other.x
				local rightdist = other.x+32 - self.x
				local topdist = self.y+32 - other.y
				local bottomdist = other.y+32 - self.y

				if leftdist < rightdist and leftdist < topdist and leftdist < bottomdist then
					self.x = other.x - 32
				end
				if rightdist < leftdist and rightdist < topdist and rightdist < bottomdist then
					self.x = other.x + 32
				end
				if topdist < leftdist and topdist < rightdist and topdist < bottomdist then
					self.y = other.y - 32
				end
				if bottomdist < leftdist and bottomdist < rightdist and bottomdist < topdist then
					self.y = other.y + 32
				end

			end
	end
end

function Enemy:hurt (damage)

	--if self.hurtCooldown <= 10 then
		self.redCooldown = 10
		self.hurtCooldown = 30
		self.health = self.health - damage
		if self.health <= 1 then self.dead = true end
	--end
end