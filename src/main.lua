collectgarbage('setstepmul', 200)
collectgarbage('setpause', 105)
math.randomseed(25565)

respath = 'ld30/res/'
srcpath = 'ld30/src/'

app = {
	width = 1280,
	height = 720,
	scale = 1,
	fullscreen = false,
	title = 'The Torment Isles',
	limitframes = true
}

dofile(srcpath..'lib.lua')
dofile(srcpath..'textures.lua')
dofile(srcpath..'sounds.lua')
dofile(srcpath..'player.lua')
dofile(srcpath..'enemy.lua')
dofile(srcpath..'tile.lua')
dofile(srcpath..'portal.lua')
dofile(srcpath..'map.lua')
dofile(srcpath..'font.lua')
dofile(srcpath..'tormentstone.lua')

offset = { x = 0, y = 0 }

tiles = {}
-- for i=1, 200 do
	
-- 	tiles[i] = {
-- 		x = (math.ceil(i/15)-1)*64,
-- 		y = (math.ceil(i%15))*32
-- 	}
-- end
-- for x=1, 32 do
-- 	for y=1, 16 do
-- 		table.insert(tiles, Tile.new(x-1, y-1))
-- 	end
-- end

player = Player.new()
portal = nil
tormentStone = nil
enemies = {}
win = false

loadMap(level[1])
for i,tile in ipairs(tiles) do print(tile.sx..', ') end

-- table.insert(enemies, Enemy.new(100, 100))
-- table.insert(enemies, Enemy.new(200, 100))
-- table.insert(enemies, Enemy.new(300, 100))
-- table.insert(enemies, Enemy.new(400, 100))

messageCooldown = 0
message = ''

function setMessage (string)

	messageCooldown = 60*3
	message = string
end

--setMessage('HELLO WORLD')

function render ()

	video.clear()

	--video.renderSprite(texture.tiles, 0, 0, 8, 8, 100, 100, 8)
	for i,tile in ipairs(tiles) do
		--video.renderSprite(texture.tiles, 0, 8, 16, 8, 10+v.x, 10+v.y, 4)
		tile:render()
	end

	-- video.renderSprite(texture.tiles, 16, 0, 8, 16, 74, 64+106, 4)
	-- video.renderSprite(texture.tiles, 16, 0, 8, 16, 74, 64+74, 4)
	-- video.renderSprite(texture.tiles, 16, 0, 8, 16, 74, 64+42, 4)
	--video.renderSprite(texture.tiles, 0, 16, 24, 24, 42, 74, 3)

	for i,enemy in ipairs(enemies) do
		enemy:render()
	end

	if not player.dead then player:render() end
	if portal ~= nil then portal:render() end
	if tormentStone ~= nil then tormentStone:render() end

	--font.render('HELLO WORLD', 10, 10, 4)
	if messageCooldown > 0 then
		font.render(message, (1280/2)-(((#message)*16)/2), 5, 2)
		video.enableTextures(false)
		video.color(0, 0, 0, 1)
		video.renderQuadz((1280/2)-(((#message)*16)/2)-10, 0, (#message)*16+15, 16+15, -10)
		--video.color(1, 1, 1, 1)
	end
	video.color(1, 1, 1, 1)

	for i = 1, player.health do
		video.renderSpritez(texture.tiles, 96, 16, 5, 5, 10+((i-1)*25), 10, 4, -10)
	end

	video.enableTextures(false)
	video.color(0, 0, 0, 1)
	video.renderQuadz(0, 0, 315, 40, -10)

	if player.dead then

		font.render('GAME OVER', (1280/2)-(((9)*64)/2), 64, 8)

		video.enableTextures(false)
		video.color(0, 0, 0, 1)
		video.renderQuadz((1280/2)-(((9)*64)/2), 64, 560, 64+20, -10)
	end

	if win then

		font.render('THANKS FOR PLAYING', (1280/2)-(((18)*64)/2), 64, 8)

		video.enableTextures(false)
		video.color(0, 0, 0, 1)
		video.renderQuadz((1280/2)-(((18)*64)/2), 64, 1140, 64+20, -10)
	end
end

function tick ()

	for i,enemy in ipairs(enemies) do
		if enemy.x+offset.x+32 > 0 and enemy.x+offset.x < 1280 then
			enemy:tick()
			if enemy.dead then table.remove(enemies, i) end
		end
	end

	player:tick()
	offset.x = (-player.x)+(app.width/2-32)
	if offset.x > 0 then offset.x = 0 end
	if offset.x < (-3072)+1280 then offset.x = (-3072)+1280 end
	--offset.y = (-player.y)+(app.height/2-32)
	if portal ~= nil then portal:tick() end

	level[levelIndex].logic()

	if messageCooldown > 0 then messageCooldown = messageCooldown - 1 end
end

function secondstep ()

	print('frames '..jellymoon.frames(), 'ticks '..jellymoon.ticks())

	if player.health < 12 then
		if player.health < 11 then player.health = player.health + 1.5
		else player.health = player.health + 1 end
	end
end

function keyDown (code)

	if not player.dead and code == keyCode.x and key[code]==false then
		player:attack()
		if tormentStone ~= nil then
			if player.x < (tormentStone.x+24)+128 and player.x+128 > (tormentStone.x+24) and player.y < (tormentStone.y+24)+128 and player.y+128 > (tormentStone.y+24) then
				win = true
				audio.play(sound.portal)
				nextLevel()
			end
		end
	end

	key[code] = true
end

function keyUp (code)

	key[code] = false
end

function mouseDown (button)
end